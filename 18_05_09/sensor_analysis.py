
# coding: utf-8

# # Hello
# 
# This is a test

# In[1]:


from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt
import re,os


# In[123]:


root_folder = '.'
image_folder = 'Flat'
image_number = '3'
def pic_path(root_folder,image_folder,image_number):
    prefix = 'trial_'+image_folder.lower()
    duration = '.*'
    number = image_number
    p=re.compile(prefix+'.*'+image_folder+'.*'+duration+'.*'+number+'\.fits')
    image_path = os.path.join(root_folder,image_folder)

    allfiles = os.listdir(image_path)
    for ifile in allfiles:
        if p.match(ifile):
            image_file = p.match(ifile).group(0)
    return os.path.join(image_path,image_file)

def load_image(image_path):
    hdu_list = fits.open(image_path)
    return hdu_list[0].data

def load_folder(root_folder,image_folder):
    image_path = os.path.join(root_folder,image_folder)
    allfiles = os.listdir(image_path)
    folder_data = []
    for ifile in allfiles:
        folder_data.append(load_image(os.path.join(image_path,ifile)))
    return np.array(folder_data)


# In[154]:


image_path = pic_path(root_folder,'Bias',image_number)
image_dataB = load_image(image_path)
image_path = pic_path(root_folder,'Dark',image_number)
image_dataD = load_image(image_path)
image_path = pic_path(root_folder,'Flat',image_number)
image_dataF = load_image(image_path)
plt.subplot(131)
plt.imshow(image_dataB)
plt.subplot(132)
plt.imshow(image_dataD)
plt.subplot(133)
plt.imshow(image_dataF)
fig = plt.gcf()
fig.set_size_inches(16.5, 5.5)
plt.show()


# In[131]:


image_path = pic_path(root_folder,'Bias',image_number)

image_data = load_image(image_path)
folder_data = load_folder(root_folder,'Bias')

plt.subplot(121)
plt.plot(image_data[100,:])
plt.subplot(122)
plt.plot(np.mean(folder_data,axis=0)[100,:])
fig = plt.gcf()
fig.set_size_inches(16.5, 5.5)
plt.show()


# In[132]:


image_path = pic_path(root_folder,'Dark',image_number)

image_data = load_image(image_path)
folder_data = load_folder(root_folder,'Dark')

plt.subplot(121)
plt.plot(image_data[100,:])
plt.subplot(122)
plt.plot(np.mean(folder_data,axis=0)[100,:])
fig = plt.gcf()
fig.set_size_inches(16.5, 5.5)
plt.show()


# In[146]:


image_path = pic_path(root_folder,'Flat',image_number)

image_data = load_image(image_path)
folder_data = load_folder(root_folder,'Flat')

plt.plot(image_data[100,:])
fig = plt.gcf()
fig.set_size_inches(16.5, 5.5)
plt.show()


# In[147]:


image_path = pic_path(root_folder,'Flat',image_number)

image_data = load_image(image_path)
folder_data = load_folder(root_folder,'Flat')

plt.plot(image_data[100,25:])
fig = plt.gcf()
fig.set_size_inches(16.5, 5.5)
plt.show()


# In[150]:


image_path = pic_path(root_folder,'Flat',image_number)

image_data = load_image(image_path)
folder_data = load_folder(root_folder,'Flat')

plt.subplot(121)
plt.plot(image_data[100,100:200])
plt.subplot(122)
plt.plot(image_data[100,150:])
fig = plt.gcf()
fig.set_size_inches(16.5, 5.5)
plt.show()


# In[167]:


image_path = pic_path(root_folder,'Flat',image_number)
image_data = load_image(image_path)
folder_data = load_folder(root_folder,'Flat')

plt.subplot(131)
plt.plot(image_data[:,500])
plt.subplot(132)
plt.plot(image_data[0:100,500])
plt.subplot(133)
plt.plot(image_data[30:,500])
fig = plt.gcf()
fig.set_size_inches(16.5, 5.5)
plt.show()


# In[162]:


image_path = pic_path(root_folder,'Bias',image_number)
image_data = load_image(image_path)
image_data_crop = image_data[30:,100:]
minval = min(image_data_crop.flatten())
maxval = max(image_data_crop.flatten())
print([minval,maxval])

plt.subplot(121)
plt.imshow(image_data_crop, cmap='gray')
plt.colorbar()
plt.subplot(122)
plt.hist(image_data_crop.flatten(),500,range=(minval,3000),log=True)

fig = plt.gcf()
fig.set_size_inches(18.5, 10.5)
plt.show()


# In[161]:


image_path = pic_path(root_folder,'Dark',image_number)
image_data = load_image(image_path)
image_data_crop = image_data[30:,100:]
minval = min(image_data_crop.flatten())
maxval = max(image_data_crop.flatten())
print([minval,maxval])

plt.subplot(121)
plt.imshow(image_data_crop, cmap='gray')
plt.colorbar()
plt.subplot(122)
plt.hist(image_data_crop.flatten(),500,range=(minval,3000),log=True)

fig = plt.gcf()
fig.set_size_inches(18.5, 10.5)
plt.show()


# In[156]:


image_path = pic_path(root_folder,'Flat',image_number)
image_data = load_image(image_path)
image_data_crop = image_data[30:,100:]
minval = min(image_data_crop.flatten())
maxval = max(image_data_crop.flatten())
print([minval,maxval])

plt.subplot(121)
plt.imshow(image_data_crop, cmap='gray')
plt.colorbar()
plt.subplot(122)
plt.hist(image_data_crop.flatten(),500,range=(minval,maxval),log=True)

fig = plt.gcf()
fig.set_size_inches(18.5, 10.5)
plt.show()

